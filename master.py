import json
import socket
from datetime import datetime
import time
import os
import sys
import subprocess
import logging
import collections
import gzip
import bz2
import twitter
import boto.sqs
import boto.sdb
import copy
import signal, time
import gc
#from boto.sqs.connection import SQSConnection
from boto.sqs.message import RawMessage
from boto.s3.connection import S3Connection
from boto.ec2.cloudwatch import CloudWatchConnection
from dateutil import parser
from boto.s3.key import Key
from timeline_parsers import WriteToFile, Slimdown, Essential, Aggregate
from utils import retry
from country import crawler_country_classifier
import conf
from partition import PartitionManager
from multiprocessing import Process
from pymongo import MongoClient

logger = logging.getLogger(__name__)

class Master:
    def __init__(self, account, sdb_domain, temp_file, q_name, b_name, crawl_type, instance_id):
        self.sqsconn = boto.sqs.connect_to_region(conf.SQS_REGION)
        self.sdbconn = None
        if conf.DATABASE == 'SDB':
            self.sdbconn = boto.sdb.connect_to_region(conf.SDB_REGION)  
        if conf.DATABASE == 'HDX':
	    self.sdbconn = 'westdb'   #for HyperDex
	if conf.DATABASE == 'MDB':
	    self.sdbconn == 'testDB'
        self.s3conn = S3Connection(conf.AWS_KEY,
                                   conf.AWS_SECRET)
        self.cwconn = CloudWatchConnection(conf.AWS_KEY,
                                           conf.AWS_SECRET)
        self.account = account
        self.parsed_tweet_file = conf.TEMP_FILE_PARSED_TIMELINE
        self.type = crawl_type
	self.id = instance_id
        if b_name == conf.S3_BUCKET_TIMELINE:
            self.log_data = True
        else:
            self.log_data = False
        self.domain = None
	if conf.DATABASE == 'SDB':
            self.domain = PartitionManager(self.sdbconn, sdb_domain, conf.DOMAIN_PARTITION_COUNT)
	if conf.DATABASE == 'HDX':
	    self.domain = PartitionManager(self.sdbconn, sdb_domain, len(conf.HYPERDEX_IP_LIST))
	if conf.DATABASE == 'MDB':
            self.domain = PartitionManager(self.sdbconn, sdb_domain, len(conf.MONGODB_IP_LIST))
        self.q = self.sqsconn.lookup(q_name)
	logger.info('1')
        self.q.set_message_class(RawMessage)
        logger.info('2')
        self.instance_id = self.get_instance_id()
        logger.info('3')
        if not os.path.exists(conf.TEMP_FOLDER):
            os.makedirs(conf.TEMP_FOLDER)

        self.users = {}
        self.users_attr = {}
        self.filename = os.path.join(conf.TEMP_FOLDER, temp_file + '_' + str(self.id))

        if crawl_type == conf.CRAWL_TYPE_TIMELINE:
            # Create timeline specific file names
	    self.slim_filename = os.path.join(conf.TEMP_FOLDER, temp_file + '_slim' + '_' + str(self.id))
            self.essential_filename = os.path.join(conf.TEMP_FOLDER, temp_file + '_ess' + '_' + str(self.id))
            self.agg_filename = os.path.join(conf.TEMP_FOLDER, temp_file + '_agg' + '_' + str(self.id))

            # Create timeline specific buckets
            self.slim_bucket = retry(lambda: self.s3conn.lookup(conf.S3_BUCKET_SLIM), "connect to slim bucket")
            self.essential_bucket = retry(lambda: self.s3conn.lookup(conf.S3_BUCKET_ESSENTIAL), "connect to essential bucket")
            self.agg_bucket = retry(lambda: self.s3conn.lookup(conf.S3_BUCKET_AGGREGATE), "connect to agg bucket")

	self.s_out = bz2.BZ2File(self.slim_filename, 'w')
        self.e_out = bz2.BZ2File(self.essential_filename, 'w')
        self.a_out = bz2.BZ2File(self.agg_filename, 'w')

        self.raw_file_size = 0
        self.users_visited = 0
        self.tweets_collected = 0
        self.last_tweet_metric_time = time.time()
        self.data_written = 0
        self.compressed_data_written = 0
        self.batch_start_utc = datetime.utcnow()
    def get_instance_id(self):
        try:
            output = subprocess.Popen(["wget", "-q", "-O", "-", "http://169.254.169.254/latest/meta-data/instance-id"],
                                    stdout=subprocess.PIPE).communicate()[0]
            return output.strip()
        except:
            return None

    def mark_crawled(self, msg):
	#logger.info('Removing %s from queue ...' % msg.get_body())
        self.q.delete_message(msg)
        #logger.info('Marked %s crawled.' % msg.get_body())
        sys.stdout.flush()

    def mark_suspended(self, account_name, code):
        try:
            account_domain = self.sdbconn.get_domain(conf.SDB_DOMAIN_ACCOUNTS)
            account_domain.put_attributes(account_name, {'is_suspended': 1, 'code': code})
        except Exception as e:
            logger.error('Failed to mark account %s as suspended.', account_name)
            logger.exception(e)
            
    def mark_denied(self, account_name, code):
        try:
            account_domain = self.sdbconn.get_domain(conf.SDB_DOMAIN_ACCOUNTS)
            account_domain.put_attributes(account_name, {'is_denied': 1, 'code': code})
        except Exception as e:
            logger.error('Failed to mark account %s as denied.', account_name)
            logger.exception(e)
    def store_data(self, data, user, p):
        """
        For timeline, return data structure would be ({ depth1: set(id1, id2, id3), depth2: set(id1, id2) }, None)
        Otherwise, the structure would be ([id1, id2], depth)
        """
        parsed_ids = set()
        depth = 0
        is_targeted = False
	item = None

        if data and user:
            item = self.domain.get_item(user)
            if item:
                #if item.get('success'):
                if ('success' in item and type(item.get('success'))==int and item.get('success')==1) or ('success' in item and type(item.get('success'))==bool and item.get('success')):
                    if conf.DATABASE == 'HDX':
                        logger.warning('Duplication tossed. User previously marked success on %s.', datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"))
                    if conf.DATABASE == 'SDB' or 'MDB':
                        logger.warning('Duplication tossed. User previously marked success on %s.', datetime.utcnow())
                    return parsed_ids, depth
                elif 'crawled_utc' in item:
                    try:
                        crawled_utc = item['crawled_utc']
			#logger.info(type(crawled_utc))
			if conf.DATABASE == 'SDB' or 'MDB':
                            if type(crawled_utc) is datetime and (datetime.utcnow() - crawled_utc).total_seconds() < 3600*3:
                                logger.warning('Duplication tossed. Upload was not found for this user yet, ' +
                                               'but user was crawled within three hours.')
                                return parsed_ids, depth
			#logger.info(crawled_utc)
                        if conf.DATABASE == 'HDX':
                            if crawled_utc is not None and crawled_utc is not '' and type(crawled_utc) is str and (datetime.utcnow() - datetime.strptime(crawled_utc,"%Y-%m-%d %H:%M:%S.%f")).total_seconds() < 3600*3:
                                logger.warning('Duplication tossed. Upload was not found for this user yet, ' +
                                               'but user was crawled within three hours.')
                                return parsed_ids, depth
                    except ValueError:
                        logger.warning('Unknown format for "crawled_utc" field. value: %s, user: %s, bucket: %s',
                                       item['crawled_utc'], user)

                if ('is_targeted' in item and item['is_targeted'] == '1') or not conf.COUNTRY_CLASSIFIER_ON:
                    is_targeted = True

                if 'depth' in item:
                    try:
                        depth = int(item['depth'])
                    except ValueError, e:
                        logger.warning('Failed to parse depth for %s. Default to 0. Detail: %s.', user, e)
                        depth = 0

	    sd = Slimdown()
            es = Essential()
            ag = Aggregate()
	    try:	
                slim = sd.slimdown_timeline(data)
                essential = es.parse_essential(slim)
                aggregate = ag.agg_tweets(essential)
	    except KeyError, e:
		logger.info('KeyError, details "%s"', str(e))
		return parsed_ids, depth
	    self.s_out.write(json.dumps(slim) + '\n')
            self.e_out.write(json.dumps(essential)+'\n')
            self.a_out.write(json.dumps(aggregate)+'\n')
            if crawler_country_classifier.classify(aggregate):
                parsed_ids = parsed_ids.union(set(aggregate['mentioned'].keys() +
                                          aggregate['in_reply_to_user_id_str'].keys() + \
                                          aggregate['retweets']['all_mentioned_in_retweets'].keys() + \
                                          aggregate['retweets']['in_reply_to_user_id_str_in_original_tweets'].keys()))

            if conf.DATABASE == 'SDB' or 'MDB':
                self.domain.put_attributes(user,
                                           {'crawled_utc': datetime.utcnow(),
                                            self.type + '_count':
                                            len(data) if type(data) is list else len(data['ids'])})
            if conf.DATABASE == 'HDX':
                self.domain.put_attributes(user,
                                           {'crawled_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"),
                                            self.type + '_count':
                                            len(data) if type(data) is list else len(data['ids'])})
	    
            self.users[user] = self.users_attr
            self.raw_file_size += len(data)
	    data = None

        # Periodically upload data to S3
	if (datetime.utcnow()-self.batch_start_utc).total_seconds() > conf.BATCH_TIMEOUT:
            logger.info('Writing %d items to s3 bucket ...', self.raw_file_size)
	    self.s_out.close()
	    self.e_out.close()
	    self.a_out.close()
            try:
                if self.raw_file_size > 0:
                    filename = str(socket.getfqdn()) + '.' + str(os.getpid())\
                        + '.' + str(datetime.now().strftime('%Y%m%d%H%M%S%f')) + '.bz2'
                    
                    if self.instance_id:
                        self.users_attr.update({
                            'instance_id': self.instance_id
                        })
                    if conf.DATABASE == 'SDB' or 'MDB':
                        self.users_attr.update({
                            'crawler_account': self.account,'upload_start_utc': datetime.utcnow(),'filename': filename
                        })
                    if conf.DATABASE == 'HDX':
                        self.users_attr.update({
                            'crawler_account': self.account,'upload_start_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"),'filename': filename
                        })

                    #self.domain.batch_put_attributes_all(self.users)

                    # Upload raw file to S3
                    # k = Key(self.bucket)
                    # k.key = filename + '.gz'
                    # k.set_contents_from_filename(self.filename)
		    succeeded_sdb = False
		    succeeded_hdx = 0
                    if self.type == conf.CRAWL_TYPE_TIMELINE:
                        try:

                            k = Key(self.slim_bucket)
                            k.key = filename
                            k.set_contents_from_filename(self.slim_filename)

                            k = Key(self.essential_bucket)
                            k.key = filename
                            k.set_contents_from_filename(self.essential_filename)

                            k = Key(self.agg_bucket)
                            k.key = filename
                            k.set_contents_from_filename(self.agg_filename)

			    succeeded_sdb = True
			    succeeded_hdx = 1
                            #if conf.COUNTRY_CLASSIFIER_ON:
                            #    p.update_is_targeted(targeted)
                        except Exception, err:
                            logger.error('Error parsing tweet file. %s', err)
                            logger.exception(err)

                    # Record upload complete for users
                    #self.users_attr.clear()
                    if conf.DATABASE == 'SDB' or 'MDB':
                        self.users_attr.update({
                            'upload_complete_utc': datetime.utcnow(), 'success': succeeded_sdb
                        })
                    if conf.DATABASE == 'HDX':
                        self.users_attr.update({
                            'upload_complete_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"), 'success': succeeded_hdx
                        })

		    #td = Process(target=self.domain.batch_put_attributes_all, args = (self.users,))
                    #td.start()
                    self.domain.batch_put_attributes_all(self.users)

            finally:
                # Create new local cache.
                self.users.clear()
                self.users_attr.clear()
		self.s_out = bz2.BZ2File(self.slim_filename, 'w')
                self.e_out = bz2.BZ2File(self.essential_filename, 'w')
                self.a_out = bz2.BZ2File(self.agg_filename, 'w')
                self.raw_file_size = 0
                self.batch_start_utc = datetime.utcnow()
        return parsed_ids, depth
    
    def push_user(self, user_id):
	if user_id=="":
	    return
        messages = []
        messages.append((0, user_id, 0))
        self.q.write_batch(messages)

    def get_user(self):
        return self.q.get_messages(1)[0]

    def tweet_count(self):
        return self.meta_table.get_item('counter')['n']

    def how_many_accounts(self):
        return 2

    def update_tweet_metrics(self):
        if time.time() - self.last_tweet_metric_time > 60:
            logger.info('updating metrics.')
            sys.stdout.flush()
            self.last_tweet_metric_time = time.time()
            dims = {'User': self.account}
            self.cwconn.put_metric_data("Twitter", "TweetsCollected", value=self.tweets_collected, dimensions=dims)
            self.cwconn.put_metric_data("Twitter", "TweetsCollected", value=self.tweets_collected) 
            self.cwconn.put_metric_data("Twitter", "UsersVisited", value=self.users_visited, dimensions=dims) 
            self.cwconn.put_metric_data("Twitter", "UsersVisited", value=self.users_visited) 
            self.cwconn.put_metric_data("Twitter", "DataWritten", value=self.data_written) 
            self.cwconn.put_metric_data("Twitter", "CompressedDataWritten", value=self.compressed_data_written) 
            self.tweets_collected = 0
            self.users_visited = 0
            self.data_written = 0
            self.compressed_data_written = 0
