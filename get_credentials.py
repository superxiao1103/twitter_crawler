import time
import random
import os
import sys
import subprocess
import threading
import shlex
import json
import logging
import logging.config
import utils
import Queue
from utils import retry
from boto.sqs.connection import SQSConnection
import boto.sdb
import boto.dynamodb
from multiprocessing import Process
import conf
if conf.DATABASE == 'HDX':
    import hyperdex.admin
    import hyperdex.client


if __name__ == '__main__':
    if not os.path.exists('log'):
        os.makedirs('log')
    with open('logging_config_launcher.json', 'rt') as f:
        config = utils.setup_logging_config(json.load(f))
    logging.config.dictConfig(config)

logger = logging.getLogger(__name__)
logger.info('Deployment Name: %s', conf.DEPLOYMENT_NAME)
logger.info('Database: %s', conf.DATABASE)


def start(id):
    tweet_command = 'python flock.py %s tweets'
    subprocess.Popen(shlex.split(tweet_command % str(id)))

def initial_space():
    for ip in conf.HYPERDEX_IP_LIST:
        a = hyperdex.admin.Admin(ip, conf.HYPERDEX_PORT)
        c = hyperdex.client.Client(ip, conf.HYPERDEX_PORT)
        space_name0='''prod1311_timeline'''
        instruction0='''space '''+space_name0+''' 
        key user_id attributes crawler_account, instance_id, filename, int truncated, int is_targeted, 
        int success, int tweets_count, upload_complete_utc, upload_start_utc, int depth, discoverd_utc, 
        crawled_utc, queued_utc, failed_utc, failed_reason, failure_code subspace crawler_account 
        create 8 partitions tolerate 2 failures'''
        try:
            logger.info('Creating prod1311_timeline space')
            a.add_space(instruction0)
        except Exception as e:
            logger.info("prod1311_timeline table exist at %s",ip)


class Launcher:
    def __init__(self):
        self.sqsconn = boto.sqs.connect_to_region(conf.SQS_REGION)
        self.q = self.sqsconn.lookup(conf.QUEUE_ACCOUNTS)
        self.sdbconn = boto.sdb.connect_to_region(conf.SDB_REGION)
        self.dom = retry(lambda: self.sdbconn.lookup(conf.SDB_DOMAIN_ACCOUNTS), "connecting to account domain")

    def get_credentials(self, instance_id):
        q = "SELECT * FROM `accounts` WHERE instance_id = '%s' and time is not NULL ORDER BY time DESC" % instance_id
        logger.info(q)
        return self.dom.select(q)

if __name__ == '__main__':
    if conf.deployment != 'DEV':
        time.sleep(10 + random.randrange(30))

    logger.info("Running get_credentials.py")

    if len(sys.argv) > 1:
        instance_id = int(sys.argv[1])
    else:
        try:
            instance_id = os.popen("/usr/local/bin/ec2-metadata -d").read().strip().split(' ')[1]
	    instance_id = int(instance_id)
            logger.info('Instance ID: ' + str(instance_id))
        except:
            logger.warning('Instance ID is not found, or is not a number. Falling back to use default value 50.')
            instance_id = 50
    if conf.DATABASE == 'HDX':
        initial_space()

    logger.info('Getting Credentials for Instance id: %s' % instance_id)
    start_id = instance_id
    for i in range(0,conf.NUM_THREADS):
	start(str(start_id+i*conf.MAX_WORKER_THREADS))
    logger.info("All crawlers started!")
