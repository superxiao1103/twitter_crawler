import time
import sys
import os
import json
import logging
import logging.config
import Queue
import signal, time
import gc
from datetime import datetime
from twitter import Twitter, OAuth
from master import Master
from populate_users import Populator
from random import randint
import threading
import conf
import utils
from email.mime.text import MIMEText
from subprocess import Popen, PIPE
from get_credentials import Launcher
from multiprocessing import Process
import psutil

TYPE = sys.argv[2]
timeleft = 180

if __name__ == '__main__':
    if not os.path.exists('log'):
        os.makedirs('log')
    with open('logging_config.json', 'rt') as logging_config:
        config = utils.setup_logging_config(json.load(logging_config))
    logging.config.dictConfig(config)

logger = logging.getLogger(__name__)
logger.info('Deployment Name: %s', conf.DEPLOYMENT_NAME)
instance_id=int(float(sys.argv[1]))

def get_followers(user, m):
    logger.info('Getting followers for user %s.', user)
    c = -1
    followers = {'user': user, 'ids': []}
    while c != 0:
        try:
            if user.isdigit():
                f = t.followers.ids(user_id=user, cursor=c)
            else:
                f = t.followers.ids(screen_name=user, cursor=c)
            c = f['next_cursor']
            followers['ids'] += f['ids']
        except Exception as e:
            try:
                code = json.loads(e.response_data)['errors'][0]['code']
            except:
                code = None

            if code == 88:
                logger.warning('Rate limit exceeded. Backing off...')
                back_off(conf.CRAWL_TYPE_FOLLOWERS)
            elif code == 64:
                logger.error('Account is suspended. Consumer key: %s.\nDetail: %s', CONSUMER_KEY, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            elif code == 32:
                logger.error('Could not authenticate. Consumer key: %s, OAuth token: %s.\nDetail: %s', CONSUMER_KEY,
                             OAUTH_TOKEN, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            else:
                logger.warning('The user is skipped because of an error. user: %s.\n Detail: %s', user, str(e))
                if conf.DATABASE == 'SDB' or 'MDB':
                    attrs = {'failed_utc': datetime.utcnow(), 'failed_reason': str(e)}
                if conf.DATABASE == 'HDX':
                    attrs = {'failed_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"), 'failed_reason': str(e)}
                try:
                    attrs['failure_code'] = code
                except Exception:
                    pass

                m.domain.put_attributes(user, attrs)

                return followers

    logger.info('Successfully get followers for user %s.', user)
    return followers


def get_friends(user, m):
    logger.info('Getting friends for user %s.', user)
    c = -1
    friends = {'user': user, 'ids': []}
    while c != 0:
        try:
            if user.isdigit():
                f = t.friends.ids(user_id=user, cursor=c)
            else:
                f = t.friends.ids(screen_name=user, cursor=c)
            c = f['next_cursor']
            friends['ids'] += f['ids']
        except Exception as e:
            try:
                code = json.loads(e.response_data)['errors'][0]['code']
            except:
                code = None

            if code == 88:
                logger.warning('Rate limit exceeded. Backing off...')
                back_off(conf.CRAWL_TYPE_FOLLOWERS)
            elif code == 64:
                logger.error('Account is suspended. Consumer key: %s.\nDetail: %s', CONSUMER_KEY, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            elif code == 32:
                logger.error('Could not authenticate. Consumer key: %s, OAuth token: %s.\nDetail: %s', CONSUMER_KEY,
                             OAUTH_TOKEN, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            else:
                logger.warning('The user is skipped because of an error. user: %s.\n Detail: %s', user, str(e))
                if conf.DATABASE == 'SDB' or 'MDB':
                    attrs = {'failed_utc': datetime.utcnow(), 'failed_reason': str(e)}
                if conf.DATABASE == 'HDX':
                    attrs = {'failed_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"), 'failed_reason': str(e)}
                try:
                    attrs['failure_code'] = code
                except Exception:
                    pass
                m.domain.put_attributes(user, attrs)

                return friends
    logger.info('Successfully get friends for user %s.', user)
    return friends

def get_tweets(user, m, t, q):
    global timeleft
    logger.info("Getting timeline for user %s.", user)
    timeline = []
    max_id_flag = False
    retry_error130 = 0 #Count the number of retries for error code 130.
    timeline_crawled = 0
    user_name = ""
    kwargs = None
    more = None
    while True:
        try:
            kwargs = {"user_id" if user.isdigit() else "screen_name": user, "count": 200}
            if max_id_flag:
                kwargs["max_id"] = long(timeline[-1]['id_str']) - long(1)
	    if int(timeleft)<=0:
		logger.info('Changing to next developer (crawler) ...')
		q,t = switch_account(q,t)
            more = t.statuses.user_timeline(**kwargs)
	    timeleft = timeleft-1
            max_id_flag = True
            if more:
		user_name = more[0]['user']['id_str']
                timeline_crawled = timeline_crawled + len(more)
                timeline += more
            else: #try one more time. Reduces truncated timeline problem.
		if int(timeleft)<=0:
		    logger.info('Changing to next developer (crawler) ...')
		    q,t = switch_account(q,t)
                more = t.statuses.user_timeline(**kwargs)
		timeleft = timeleft-1
                if more:
                    timeline_crawled = timeline_crawled + len(more)
                    timeline += more
                else:
		    try:
                        trunc_rate=float(timeline_crawled)/min( 3200, float(timeline[0]['user']['statuses_count']) )
                    except: #handle zero division exception
                        trunc_rate=0.
                    if trunc_rate < 0.9:
                        attrs = {'truncated': 1}
                        logger.info('truncated, m.domain.put_attributes(user, attrs)...')
                        m.domain.put_attributes(user_name, attrs)
                    break
        except Exception as e:
            try:
                code = json.loads(e.response_data)['errors'][0]['code']
            except:
                code = None

	    if code == 88:
                logger.warning('Rate limit exceeded. Backing off...')
                logger.info('Changing to next developer (exception) ...')
		q,t = switch_account(q,t)
            elif code == 64:
		timeleft = timeleft-1
                logger.error('Account is suspended. Consumer key: %s.\nDetail: %s', CONSUMER_KEY, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            elif code == 32:
		timeleft = timeleft-1
                logger.error('Could not authenticate. Consumer key: %s, OAuth token: %s.\nDetail: %s', CONSUMER_KEY,
                             OAUTH_TOKEN, str(e))
                logger.exception(e)
                m.mark_suspended(USER, code)
                logging.shutdown()
                exit(1)
            elif code == 89:
		timeleft = timeleft-1
                logger.error('The access token used in the request is incorrect or has expired. OAuth token: %s.\nDetail: %s',
                             OAUTH_TOKEN, str(e))
                logger.exception(e)
                notify_failure( str(e) ) #Send a notification email to cnpcornell@gmail.com that the account is no longer valid.
                logging.shutdown()
                exit(1)
                #TODO: Retire this Twitter developer account and restart with another valid account.
            elif code == 130:
		timeleft = timeleft-1
                retry_error130 += 1 #increment for each retry that we initiate due to Twitter overcapacity
                if retry_error130 < 3: #retry 2 times.
                    logger.warning('Twitter is currently over capacity. Backing off for 2 seconds.')
                    time.sleep(2) #retry after 2 seconds
                else:
                    return get_tweet_default_error_handler(user, e, code, m, timeline) #I refractored the default error handling code into a new function. See below.
            else:
		timeleft = timeleft-1
                #return get_tweet_default_error_handler(user, e, code, m, timeline)
		get_tweet_default_error_handler(user, e, code, m, timeline)
		break
    logger.info('Successfully get timeline for user %s.', user)
    return timeline,t,q

def switch_account(q,t):
    global timeleft
    result = q.get()
    q.put(result)
    result = [(str(i.name), i['oauth_key'], i['oauth_secret'], i['token'], i['secret']) for i in result]
    args = result[0]
    new_CONSUMER_KEY = args[1]
    new_CONSUMER_SECRET = args[2]
    new_OAUTH_TOKEN = args[3]
    new_OAUTH_SECRET = args[4]
    t = None
    t = Twitter(
        auth=OAuth(new_OAUTH_TOKEN, new_OAUTH_SECRET,
            new_CONSUMER_KEY, new_CONSUMER_SECRET)
    )
    logger.info(args)
    try:
        timeleft = int(t.application.rate_limit_status()['resources']["statuses"]["/statuses/user_timeline"]['remaining'])
    except Exception:
	timeleft = 0
    logger.info('We have %d calls available now',int(timeleft))
    while timeleft != 180:
	logger.info('All accounts are blocked.')
        back_off(conf.CRAWL_TYPE_TIMELINE,t)
	timeleft = int(t.application.rate_limit_status()['resources']["statuses"]["/statuses/user_timeline"]['remaining'])
    return q,t

def get_tweet_default_error_handler(user, e, code, m, timeline):
    logger.warning('The user is skipped because of an error. user: %s.\n Detail: %s', user, str(e))
    if conf.DATABASE == 'SDB' or 'MDB':
        attrs = {'failed_utc': datetime.utcnow(), 'failed_reason': str(e)}
    if conf.DATABASE == 'HDX':
        attrs = {'failed_utc': datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"), 'failed_reason': str(e)}
    try:
        attrs['failure_code'] = str(code)
    except Exception:
        pass
    #logger.info(type(user))
    #logger.info(type(attrs))
    #logger.info(attrs)
    m.domain.put_attributes(str(user), attrs)
    return
    
def notify_failure(m):
    pass
    # TODO: Re-enable when account is banned, send email. cnpcornell@gmail.com
    msg = MIMEText(m)
    msg["From"] = conf.NOTIFICATION_FROM
    send_to_list = conf.NOTIFICATION_TO
    for i in range(len(send_to_list)):
        msg["To"] = send_to_list[i]
        msg["Subject"] = 'A failure has occurred on account %s' % USER
        p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
        p.communicate(msg.as_string())
    return


def back_off(type, t):
    try:
        status = t.application.rate_limit_status()
        res = "statuses" if type == conf.CRAWL_TYPE_TIMELINE else \
            "friends" if type == conf.CRAWL_TYPE_FRIENDS else \
            "followers" if type == conf.CRAWL_TYPE_FOLLOWERS else None
        api = "/statuses/user_timeline" if type == conf.CRAWL_TYPE_TIMELINE else \
            "/friends/ids" if type == conf.CRAWL_TYPE_FRIENDS else \
            "/followers/ids" if type == conf.CRAWL_TYPE_FOLLOWERS else None
        reset = status['resources'][res][api]['reset']
        b = reset - time.time() + 10
	if b<0:
	    logger.error('negative sleep time, changed to 300s ...')
	    b = 300
    except Exception as e:
        b = 300
        logger.info('Waiting %d seconds due to API error: %s' % (b, str(e)))
    time.sleep(b)
    return


def start(q,t):
    global instance_id
    try:
        if TYPE == conf.CRAWL_TYPE_TIMELINE:
            m = Master(sdb_domain=conf.SDB_DOMAIN_TIMELINE,
                       temp_file=conf.TEMP_FILE_TIMELINE,
                       q_name=conf.QUEUE_TIMELINE,
                       b_name=conf.S3_BUCKET_TIMELINE,
                       account=USER,
                       crawl_type=TYPE,
		       instance_id=instance_id)
        elif TYPE == conf.CRAWL_TYPE_FRIENDS:
            m = Master(sdb_domain=conf.SDB_DOMAIN_FRIENDS,
                       temp_file=conf.TEMP_FILE_FRIENDS,
                       q_name=conf.QUEUE_FRIENDS,
                       b_name=conf.S3_BUCKET_FRIENDS,
                       account=USER,
                       crawl_type=TYPE,
                       instance_id=instance_id)
        elif TYPE == conf.CRAWL_TYPE_FOLLOWERS:
            m = Master(sdb_domain=conf.SDB_DOMAIN_FOLLOWERS,
                       temp_file=conf.TEMP_FILE_FOLLOWERS,
                       q_name=conf.QUEUE_FOLLOWERS,
                       b_name=conf.S3_BUCKET_FOLLOWERS,
                       account=USER,
                       crawl_type=TYPE,
                       instance_id=instance_id)
        else:
            logger.fatal('Unsupported crawler type: %s. The crawler is shutting down.', TYPE)
            logging.shutdown()
            exit(1)

        p = Populator()
    except Exception, e:
        logger.exception(e)
        logger.fatal('The crawler is shutting down, because of an error in initialization.')
        exit(1)
    logger.info('mark1')
    while True:
        try:
            try:
                msg = m.get_user()
            except IndexError:
                logger.info('No more messages. Sleeping for 15 minutes...')
                m.store_data([], None, p)
                time.sleep(900)
                continue
            user = msg.get_body()
            if TYPE == conf.CRAWL_TYPE_TIMELINE:
		r,t,q = get_tweets(user, m, t, q)
		gc.collect()
            elif TYPE == conf.CRAWL_TYPE_FRIENDS:
                r = get_friends(user, m)
            elif TYPE == conf.CRAWL_TYPE_FOLLOWERS:
                r = get_followers(user, m)
            else:
                logging.fatal("Unsupported crawl type: %s. The crawler is shutting down.", TYPE)
                logging.shutdown()
                exit(1)
	    ids, depth = m.store_data(r, user, p) #update SDB and pushing data to S3 #TODO
	    gc.collect()
	    m.mark_crawled(msg)
            if ids:
		p._populate_users(users=ids, depth=depth+1)
		#td = threading.Thread(target=queuing, args = (ids,p,depth))
		#td = Process(target=queuing, args = (ids,p,depth))
                #td.start()
	    ids = None
	    depth = None
	    r = None
	    user = None
	    msg = None
	    collected = None
	    logger.info("Memory:     "+str(memory_usage_psutil()))
        except Exception as e:
            logger.error(e)
            logger.warning("Unhandled error occurred. The crawler will resume in 60 seconds...")
            time.sleep(60)

def memory_usage_psutil():
    # return the memory usage in MB
    process = psutil.Process(os.getpid())
    mem = process.get_memory_info()[0] / float(2 ** 20)
    return mem

def queuing(ids,p,depth):
    p.populate_users(users=ids, depth=depth+1, async=True)
    #p._populate_users(users=ids, depth=depth+1)
    
if __name__ == '__main__':
    q = Queue.Queue()
    m = Launcher()
    instance_id=int(float(sys.argv[1]))
    try:
        start_id = os.popen("/usr/local/bin/ec2-metadata -d").read().strip().split(' ')[1]
    except:
        start_id = 50
    if instance_id-start_id >= int(conf.NUM_THREADS)*int(conf.MAX_WORKER_THREADS):
        num_accounts = conf.ADD_WORKER_THREADS
    else:
        num_accounts = conf.MAX_WORKER_THREADS
    for i in range(0, num_accounts):
        logger.info('Getting Credentials for Instance id: %d' % instance_id)
        more_account = m.get_credentials(instance_id)
        instance_id = instance_id + 1
        q.put(more_account)

    result = q.get()
    q.put(result)
    result = [(str(i.name), i['oauth_key'], i['oauth_secret'], i['token'], i['secret']) for i in result]
    args = result[0]
    USER = args[0]
    CONSUMER_KEY = args[1]
    CONSUMER_SECRET = args[2]
    OAUTH_TOKEN = args[3]
    OAUTH_SECRET = args[4]
    t = Twitter(
        auth=OAuth(OAUTH_TOKEN, OAUTH_SECRET,
            CONSUMER_KEY, CONSUMER_SECRET)
    )
    logger.info(args)
    timeleft = int(t.application.rate_limit_status()['resources']["statuses"]["/statuses/user_timeline"]['remaining'])
    logger.info('We have %d calls available now',int(timeleft))
    while True:
        try:
            start(q,t)
        except Exception as e:
	    logger.info('Exception happens at main loop, details: \n',str(e))
	    time.sleep(30)
    logger.info('Ends')
