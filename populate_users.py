"""
This script clear the current user queues populates them with the seed user list.
"""
from datetime import datetime
from multiprocessing.pool import ThreadPool
from multiprocessing import Queue
import os
import logging
import json
import sys
import time
import threading
import gc
from dateutil import parser
from boto.sqs.connection import SQSConnection
from boto.sqs.message import RawMessage
import boto
import boto.sdb
from pymongo import MongoClient
from utils import setup_logging_config, retry
from partition import PartitionManager
from memoized import memoized_ttl
import conf
#from pymongo import MongoClient


if __name__ == '__main__':
    if not os.path.exists('log'):
        os.makedirs('log')
    with open('logging_config.json', 'rt') as logging_config:
        config = setup_logging_config(json.load(logging_config))
    logging.config.dictConfig(config)

logger = logging.getLogger(__name__)
#LOG_FILENAME = 'logging_rotatingfile_example'
#logger.setLevel(logging.DEBUG)
#handler = logging.handlers.RotatingFileHandler(
#              LOG_FILENAME, maxBytes=2024, backupCount=5)
#logger.addHandler(handler)

class Populator:
    lock = threading.Lock()

    def __init__(self):
        logger.info("Establishing SQS connection.")
        #self.sqsconn = SQSConnection(conf.AWS_KEY,conf.AWS_SECRET)
        self.sqsconn = boto.sqs.connect_to_region(conf.SQS_REGION)

        logger.info("Creating queues...")
        #self.friend_q = self.sqsconn.create_queue(conf.QUEUE_FRIENDS)
        #self.follower_q = self.sqsconn.create_queue(conf.QUEUE_FOLLOWERS)
        self.follower_q = self.friend_q = None
        self.timeline_q = self.sqsconn.lookup(conf.QUEUE_TIMELINE)

        #self.friend_q.set_message_class(RawMessage)
        #self.follower_q.set_message_class(RawMessage)
        self.timeline_q.set_message_class(RawMessage)
        ''''
        self.sdbconn = retry(lambda: boto.sdb.connect_to_region(conf.SDB_REGION,
                                                                aws_access_key_id=conf.AWS_KEY,
                                                                aws_secret_access_key=conf.AWS_SECRET),
                             "connect to sdb")
        '''
        #TODO
        self.sdbconn = None
        if conf.DATABASE == 'SDB':
            self.sdbconn = boto.sdb.connect_to_region(conf.SDB_REGION)
        if conf.DATABASE == 'HDX' or 'MDB':
	    self.sdbconn = 'westdb' #HyperDex
        
        logger.info("Creating SDB domains...")
        #self.friend_d = PartitionManager(self.sdbconn, conf.SDB_DOMAIN_FRIENDS, conf.DOMAIN_PARTITION_COUNT)
        #self.follower_d = PartitionManager(self.sdbconn, conf.SDB_DOMAIN_FOLLOWERS, conf.DOMAIN_PARTITION_COUNT)
	if conf.DATABASE == 'SDB':
            self.timeline_d = PartitionManager(self.sdbconn, conf.SDB_DOMAIN_TIMELINE, conf.DOMAIN_PARTITION_COUNT)
	if conf.DATABASE == 'HDX':
	    self.timeline_d = PartitionManager(self.sdbconn, conf.SDB_DOMAIN_TIMELINE, len(conf.HYPERDEX_IP_LIST))
	if conf.DATABASE == 'MDB':
            self.timeline_d = PartitionManager(self.sdbconn, conf.SDB_DOMAIN_TIMELINE, len(conf.MONGODB_IP_LIST))

        self.pool = ThreadPool(conf.MAX_WORKER_THREADS)
        self.pool_queue = Queue()
        logger.info("Connected.")

    def clear_queues(self):
        logger.info("Clear queues...")
        #self.friend_q.clear()
        #self.follower_q.clear()
        self.timeline_q.clear()
        logger.info("Successfully cleared queues.")

    @staticmethod
    def should_add(q, domain, user_id):
        actions = []

        try:
            # if not item or not item.get('discovered_utc'):
            #     ''' If user hasn't been discovered, then discover. '''
            #     actions.append('Discover')

            if not Populator._queue_limit_exceeded(q):
                with Populator.lock:    # Make sure thread-safety
                    item = domain.get_item(user_id)
                if conf.DATABASE == 'SDB':
                    if (not item or 'queued_utc' not in item or 
                        (datetime.utcnow() - parser.parse(item.get("queued_utc"))).days > conf.SQS_RETENTION_DAYS):
                        ''' If queue isn't full, and
                            user hasn't been discovered, or queued, or has been removed from queue,
                            then queue.'''
                        actions.append('Queue')
                if conf.DATABASE == 'HDX':
                    if (not item or 'queued_utc' not in item or 
                        (datetime.utcnow() - datetime.strptime(item.get("queued_utc"),"%Y-%m-%d %H:%M:%S.%f")).days > conf.SQS_RETENTION_DAYS):
                        ''' If queue isn't full, and
                            user hasn't been discovered, or queued, or has been removed from queue,
                            then queue.'''
                        actions.append('Queue')
		if conf.DATABASE == 'MDB':
                    if (not item or 'queued_utc' not in item or
                        (datetime.utcnow() - item.get("queued_utc")).days > conf.SQS_RETENTION_DAYS):
                        ''' If queue isn't full, and
                            user hasn't been discovered, or queued, or has been removed from queue,
                            then queue.'''
                        actions.append('Queue')
        except Exception, e:
            logger.warning("Ignored exception in should_add. %s", e)

        return actions

    @staticmethod
    def add_message(q, domain, user_id, crawl_type, depth):
        msg = RawMessage(body=user_id)

        with Populator.lock:    # Make sure thread-safety
            retry(lambda: q.write(msg), "Add message %s to %s queue" % (user_id, crawl_type))
            #TODO
            if conf.DATABASE == 'SDB' or 'MDB':
                domain.put_attributes(user_id, {"queued_utc": datetime.utcnow(), "depth": depth})
            if conf.DATABASE == 'HDX':
                domain.put_attributes(user_id, {"queued_utc": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"), "depth": depth})
	msg = None

    @staticmethod
    @memoized_ttl(60)
    def _queue_limit_exceeded(q):
        return q.count() > conf.MAX_QUEUE_SIZE

    @staticmethod
    def _add_messages_batch(q, domain, user_ids, crawl_type, actions_override=[], depths={}, is_targeted=[]):
        if not q:
            return

        messages = []
        attributes = {}
        for index, user_id in enumerate(user_ids):
            if not actions_override:
                actions = Populator.should_add(q, domain, user_id)
            else:
                actions = actions_override

            if actions:
                attributes[user_id] = {}
                # if 'Discover' in actions:
                #     attributes[user_id].update({"discovered_utc": datetime.utcnow()})
                if 'Queue' in actions:
                    messages.append((index, user_id, 0))
                    if conf.DATABASE == 'SDB' or 'MDB':
                        attributes[user_id].update({"queued_utc": datetime.utcnow(),
                                                    "depth": depths[user_id]})
                    if conf.DATABASE == 'HDX':
                        attributes[user_id].update({"queued_utc": datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"),
                                                    "depth": depths[user_id]})
                if 'UpdateIsTargeted' in actions:
                    attributes[user_id].update({"is_targeted": int(is_targeted[user_id])})

            if len(messages) >= 10:
                with Populator.lock:
                    if len(messages) > 0:
                        retry(lambda: q.write_batch(messages), "Batch add messages to %s queue" % crawl_type)
                        messages = []

            if len(attributes) >= 1000:
                #TODO
                with Populator.lock:
                    domain.batch_put_attributes_all(attributes)
                    attributes = {}

            if (index % 50) == 0:
                logger.info("%d: Sending %s", index, user_id)
        # write the last batch.
        if len(messages) > 0:
            with Populator.lock:
                retry(lambda: q.write_batch(messages), "Batch add messages to %s queue" % crawl_type)
        if len(attributes) > 0:
            #TODO
            with Populator.lock:
                domain.batch_put_attributes_all(attributes)

	index = None
	user_id = None
	actions = None
	messages = None
	attributes = None

    def populate_users(self, filename=None, users=None, depth=0, async=False):
	self.pool.apply_async(self._populate_users, (filename, users, depth))
	'''
        if async and ((users and len(users) > 10) or filename):
            self.pool.apply_async(self._populate_users, (filename, users, depth))
        else:
            self._populate_users(filename, users, depth)
	'''

    def _populate_users(self, filename=None, users=None, depth=0):
        if not filename and not users:
            return False
        elif filename is not None:
            if users is None:
                users = open(filename, 'r')
            else:
                logger.warning('users and filename cannot be given at the same time.')
                return False

        index = 0
        depths = {}
	
        try:
            for index, user in enumerate(users):
                if user:
                    if type(user) is not str:   # Clean up for string input
                        user = str(user)
                    elif user.isspace():                   # For non-str type, convert to str.
                        continue
                    else:
                        user = user.strip()

                    depths[user] = depth

                if len(depths) >= 500:
                    try:
                        ids = depths.keys()
                        #Populator._add_messages_batch(self.friend_q, self.friend_d, ids, "friend", depths=depths)
                        #Populator._add_messages_batch(self.follower_q, self.follower_d, ids, "follower", depths=depths)
                        Populator._add_messages_batch(self.timeline_q, self.timeline_d, ids, "timeline", depths=depths)
                    except Exception, e:
                        logger.warning("Failed to add %d users to queue. The error is ignored. Detail: %s", len(ids), e)
                        logger.exception(e)
                    finally:
                        depths = {}
        except Exception, e:
            logger.warning("Exception occurred while adding users to queue. Detail: %s", e)
            logger.exception(e)
            return False
        finally:
            if filename is not None:
                users.close()
	
        if len(depths) > 0:
            try:
                ids = depths.keys()
                #Populator._add_messages_batch(self.friend_q, self.friend_d, ids, "friend", depths=depths)
                #Populator._add_messages_batch(self.follower_q, self.follower_d, ids, "follower", depths=depths)
                Populator._add_messages_batch(self.timeline_q, self.timeline_d, ids, "timeline", depths=depths)#TODO
            except Exception, e:
                logger.warning("Failed to add %d users to queue. The error is ignored. Detail: %s", len(ids), e)
                return False

        logger.info("Finished sending %d users to queues.", index)
	
        ids = None
	depth = None
	depths = None
	filename = None
	index = None
	users = None
	'''
	collected = gc.collect()
        logger.info("Garbage collector in populate process: collected %d objects.", collected)
	'''

    def update_is_targeted(self, d):
        if type(d) == dict:
            #Populator._add_messages_batch(self.friend_q, self.friend_d, d.keys(), "friend",
            #                              actions_override=["UpdateIsTargeted"],
            #                              is_targeted=d)
            #Populator._add_messages_batch(self.follower_q, self.follower_d, d.keys(), "follower",
            #                              actions_override=["UpdateIsTargeted"],
            #                              is_targeted=d)
            Populator._add_messages_batch(self.timeline_q, self.timeline_d, d.keys(), "timeline",
                                          actions_override=["UpdateIsTargeted"],
                                          is_targeted=d)

if __name__ == '__main__':
    if len(sys.argv) in range(2, 5):
        p = Populator()
        logger.info("The script populates the crawler queues with the seed user list.")
        logger.info("Seed list file: %s" % sys.argv[-1])
        logger.info("Deployment: %s" % conf.DEPLOYMENT_NAME)
        if '-clean' in sys.argv:
            if '-f' not in sys.argv:
                s = raw_input(("Warning: The operation will DELETE all current messages in queues %s, %s and %s. " +
                              "Continue? (y/n)")
                              % (conf.QUEUE_FRIENDS, conf.QUEUE_FOLLOWERS, conf.QUEUE_TIMELINE))
                if s not in ['Y', 'y']:
                    exit(0)
            else:
                sys.argv.remove('-f')
                logger.info("The queue will be cleared in 10 seconds...")
                for i in range(9, -1, -1):
                    time.sleep(1)
                    print i
            p.clear_queues()

        p.populate_users(filename=sys.argv[-1])
    else:
        logger.error("Unexpected number of input args.")
        print "Usage:"
        print "    python populate_users.py path/to/user/list"
        print "    python populate_users.py -clean path/to/user/list"
        print "    python populate_users.py -clean -f path/to/user/list"
        exit(1)

