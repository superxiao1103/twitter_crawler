'''
Created on Oct 25, 2013
Classify country using either unions or intersections of timezone, language, and tweet_location attributes.
There are two types of target countries. "intersection_target" and "union_target".

The "intersection_target" countries are countries where dominant language is spoken in other countries (e.g. UK, France).
These countries cannot be identified by language along, so, we check if (language and timezone) or (timezone and location), or (language and location) are consistent.
For example, if language = 'en' and timezone = 'GB', we consider this user to be from the UK. language = 'en' alone cannot identify the user as a British user.

The "union_target" countries are the countries where the dominant language is used only in that country. (e.g. Korea).
These countries can be identified by language alone. Hence, if a user's location=='KR', language=='kr', or timezone == 'Seoul', we can safely classify this user
as a Korean user.

@author: shishong
'''

import json
import os
import conf
#from pprint import pprint

def decode_json(fname):  #file with user aggregate info on each line.
    with open(fname, 'r') as f1:
        for line in f1:
            line = line.strip()
            d= json.loads(line)
            yield d
            
class Mappings(): #country code and language code dictionaries that map to country codes

    def tzone_mapper(self, fname): #dict: key = twitter timezone (e.g. "Tokyo"), value = list of candidate countries (two letter country code)
        out = {}
        with open(fname, 'r') as d:
            for line in d:
                if line[0] != "#":
                    line = line.strip()
                    k = line.split('\t')
                    zname = k.pop(0)
                    out[ zname ] = k
        return out

    
    def country_code_mapper(self, fname): #dict: key = country code (2-letter), value= name of country
        dic = {}
        m = open(fname, 'r')
        for line in m:
            line = line.strip()
            name, c2, c3, k = line.split('\t')
            dic[c2] = name
        m.close()
        return dic

    def map_langcode_to_langname(self, lang_code_file): #2 or 3 letter language code (ISO639) to language name.
        map0 = {}
        with open(lang_code_file, 'r') as lang_map:
            for line in lang_map:
                line = line.strip()
                alpha3, alpha3_term, alpha2, en_name, fr_name = line.split('|')
                if len(alpha2) == 2: #Some language entries don't have alpha2 representation
                    map0[alpha2] = en_name.split(';')
                else:
                    map0[alpha3] = en_name.split(';')
        return map0

    def map_userlang_to_langname(self, user_lang_list): #user_lang_list is the list of all languages supported by Twitter's UI. 
        map0 = {}
        with open(user_lang_list, 'r') as lang_map:
            for line in lang_map:
                line = line.strip()
                desc, code = line.split('(')
                code = code[:-1]
                map0[code] = desc.strip()
        return map0

    def map_lang_country(self, filename):
        #First returned dict: Map the official language, languages with higher than 10% users in country, and "lingua franca" of a country.
        #Second returned dict: key = one language code, value = all countries (alpha2) that use that language.
        #Input file: File combining information from the CIA World Factbook and tne geonames.org data
        
        result = [{}, {}]  #First dictionary will for just frequently used (and official, and "lingua Franca") languages. Second dict will be for all languages.
        
        with open(filename, 'r') as s1:
            t=0
            for line in s1:
                if t != 0:
                    line = line[:-1]
                    d = line.split('\t')
                    cname = d[0].strip("\"\'")
                    if d[2] != "":
                        c_alpha2 = d[2]
                        freq_lang = d[3].strip("\'").split(',')
                        all_lang = d[4].strip("\'").split(',')
                        
                        container = [freq_lang, all_lang]
                        for k in range( len(container) ):
                            for i in container[k]:
                                i= i.strip().strip("\"\'")
                                if i not in result[k]:
                                    result[k][i] = set()
                                result[k][i].add( c_alpha2 )
                                
                                #Get rid of trailing country code (e.g. -GB) and add values to the two letter item.
                                if "-" in i:
                                    lcode, ccode = i.split("-")
                                    if lcode not in result[k]:
                                        result[k][lcode] = set()
                                    result[k][lcode].add( c_alpha2 )                
                t += 1
        
        #print result[1]['jv']
        return result

"""
def user_lang( user ): #self-declared user interface language
    #Non-standard (2-letter) language codes in user_lang: fil (filipino), lolc (lolcatz), msa (Malay).
    # standard, but non 2-letter language codes: en-gb (British English), zh-cn (Simplified Chinese), zh-tw (Traditional Chinese)
    return user["user_lang"].keys()

def user_location( user ):
    return user["user_location"].keys()[0]
"""


class ClassifyCountry():
    def __init__(self, tz_file=None, lang_to_country_file=None, country_code_file=None, lang_code_file=None, twitter_UI_lang_file=None): #target_countries = list of country codes that crawler should target.
        m = Mappings()
            
        if tz_file:
            self.timezone_encode = m.tzone_mapper( tz_file ) #twitter's timezone to country code (2-letter)
             
        if lang_to_country_file:
            self.frequent_lang_countries, self.all_lang_countries = m.map_lang_country( lang_to_country_file )
            
        if country_code_file:
            self.country_code_decode = m.country_code_mapper( country_code_file ) 
            
        if lang_code_file: #optional: maps ISO language codes to language names.
            self.lang_code_decode = m.map_langcode_to_langname( lang_code_file )
            
        if twitter_UI_lang_file:
            self.twitter_UI_lang_decode = m.map_userlang_to_langname( twitter_UI_lang_file )
            
        
    def user_time_zone(self, user): #one_user is a json decoded tweet aggregation object for an individual user
        return user["user_time_zone"].keys()[0]  # (user_id string, [user time zone strings])

    def _n_most_values(self, d, n_values, proportion_threshold):  
        """user_attrib such as user['tweet_lang'] or user['tweet_location']. 
        n_values determines the n most frequent values, 
        proportion threshold determines the minimum fraction a value should be"""
        
        if 'und' in d: # 'und' is undefined (language)
            del d['und']
        if '' in d:
            del d[''] #TODO: Identify the reason why there are '' in geolocation and tweet language.
            
        sum1 = sum(d.values()) #number of tweets with location information.
        if len( d ) > 0:
            attrib = []
            t=0
            
            for w in sorted(d, key=d.get, reverse=True): #sort keys in descending "value"
                if n_values == 1: #location with maximum frequency
                    attrib.append(w)
                    break
                else:
                    if t < n_values:
                        if t==0:
                            attrib.append(w)
                        else:
                            if d[w] / float(sum1) > proportion_threshold:
                                attrib.append(w)
                        t+=1
            return attrib
        
        else: #If there was no location information
            return []
    
    def tweet_lang(self, user, n_langs, proportion_threshold):
        return self._n_most_values(d= user['tweet_lang'], n_values = n_langs, proportion_threshold = proportion_threshold)

    def tweet_location(self, user, n_locations, proportion_threshold):
        return self._n_most_values(d= user['tweet_location'], n_values = n_locations, proportion_threshold = proportion_threshold)


    def unions(self, sets): # return the union set() of the countries associated with the user's language, location, and timezone.
        union0 = set()
        for i,j in sets.iteritems():
            union0.update( j )
        return union0
    
    
    def intersections(self, sets): # return the two-way intersections of tz, tweet_lang, and tweet_loc.
        tz_lang_intersect = sets['tz'].intersection(sets['tweet_lang'])
        tz_loc_intersect = sets['tz'].intersection(sets['tweet_loc'])
        lang_loc_intersect = sets['tweet_lang'].intersection(sets['tweet_loc'])
        return tz_lang_intersect, tz_loc_intersect, lang_loc_intersect
    
    
    def country_sets(self, user, n_langs, n_locations, proportion_threshold):
        try:
            tz_country_set = set( self.timezone_encode[ self.user_time_zone( user ) ] )
        except KeyError:
            tz_country_set = set()
        
        tweet_lang_list = self.tweet_lang(user, n_langs, proportion_threshold)
        
        lang_country_set = set()
        if len( tweet_lang_list ) > 0:
            for i in tweet_lang_list:
                try:
                    lang_country_set.update( self.frequent_lang_countries[i] ) #add tweet languages' corresponding countries to union0
                except KeyError:
                    pass       
        tweet_loc_country_set = set( self.tweet_location( user, n_locations, proportion_threshold ) )     
        return {'tz': tz_country_set, 'tweet_lang': lang_country_set, 'tweet_loc': tweet_loc_country_set}                       
    
    
    def user_in_target_country(self, user, union_target, intersection_target, tz_only_target, n_langs, n_locations, proportion_threshold): # return True if there is at least one country overlapping in the union set and the target country list
        sets = self.country_sets( user, n_langs, n_locations, proportion_threshold )
        
        #Check if user is in one of the union_target countries
        union0 = self.unions( sets )
        union_match = union0.intersection( set( union_target ) )
        
        """
        if len(union_match) > 0:
            #print "union", union
            #print "target countries", self.target_countries
            print "union match", union_match
            print "user timezone", self.user_time_zone( user )
            print "tweet languages", self.tweet_lang(user, n_langs, proportion_threshold)
            print "tweet locations", self.tweet_location( user, n_locations, proportion_threshold )
            print
        """
        
        #Check if the user is in one of the intersection_target countries.
        intersection_match = 0
        tz_lang, tz_loc, lang_loc = self.intersections(sets)
        if len( tz_lang.intersection( set(intersection_target) ) ) > 0 or len( tz_loc.intersection( set(intersection_target) ) ) > 0 or len( lang_loc.intersection( set(intersection_target) ) ) > 0:
            intersection_match = 1
            """
            print "intersection match", tz_lang.intersection( set(intersection_target) ), tz_loc.intersection( set(intersection_target) ), lang_loc.intersection( set(intersection_target) )
            print "user timezone", self.user_time_zone( user )
            print "tweet languages", self.tweet_lang(user, n_langs, proportion_threshold)
            print "tweet locations", self.tweet_location( user, n_locations, proportion_threshold )
            print
            """
  
        if len(union_match)>0 or intersection_match == 1:
            return True
        elif len(tz_only_target) > 0 and len( sets['tz'].intersection( set(tz_only_target) ) ) > 0:
            return True
        else:
            return False

#countries where at least one of timezone, language, or location of a user agree with country characteristic.
union_target = ['NL', 'ID', 'JP', 'KR', 'TR']

#countries where (timezone and language) or (timezone and location) or (language and location) agree with country.
intersection_target = ['FR', 'SG', 'GB']

tz_only_target = ['US']

module_path = os.path.realpath(__file__)
d = os.path.dirname(module_path)
timezone_mapfile = os.path.join(d, "twitter_timezones_corrected.txt")
ui_lang_list = os.path.join(d, "twitter_user_lang_list.txt")
lang_code_file = os.path.join(d, "ISO-639-2_utf-8.txt")
lang_to_country_file = os.path.join(d, "language_by_country_CIA_factbook_and_Geonames_org.txt")
country_code_file = os.path.join(d, "country_code_alpha23.txt")
c = ClassifyCountry(tz_file=timezone_mapfile,
                    lang_to_country_file=lang_to_country_file,
                    country_code_file=country_code_file,
                    lang_code_file=lang_code_file,
                    twitter_UI_lang_file=ui_lang_list)


def classify(agg_user):
    if conf.COUNTRY_CLASSIFIER_ON:
        return c.user_in_target_country(agg_user,
                                        union_target=union_target,
                                        intersection_target=intersection_target,
                                        tz_only_target=tz_only_target,
                                        n_langs=2,
                                        n_locations=2,
                                        proportion_threshold=0.3)
    else:
        return True


