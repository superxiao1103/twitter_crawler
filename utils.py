import logging
import os
import time
from datetime import datetime
from os.path import basename

import __main__


logger = logging.getLogger(__name__)


def setup_logging_config(conf):
    """ Replace %(process)d in the "filename" attribute with actual process id.
        This is to allow multiple crawler processes to run and each has its own log file. """
    if "handlers" in conf:
        handlers = conf["handlers"]
        for i in handlers.keys():
            if "filename" in handlers[i]:
                if "%(process)d" in handlers[i]["filename"]:
                    handlers[i]["filename"] = \
                        handlers[i]["filename"].replace("%(process)d", str(os.getpid()))
                if "%(filename)s" in handlers[i]["filename"]:
                    handlers[i]["filename"] = \
                        handlers[i]["filename"].replace("%(filename)s",
                                                        os.path.splitext(basename(__main__.__file__))[0])
                if "%(time)s" in handlers[i]["filename"]:
                    handlers[i]["filename"] = \
                        handlers[i]["filename"].replace("%(time)s",
                                                        datetime.now().strftime('%Y%m%d%H%M%S'))
    return conf


def retry(operation, desc, retry_count=5, delay=[10, 60, 300, 600]):
    for i in range(0, retry_count):
        try:
            return operation()
        except Exception as ex:
            logger.warning("Error: %s", str(ex))

        if i < retry_count-1:
            logger.warning("Failed to %s. Retry after %d seconds...", desc, delay[i])
            time.sleep(delay[i])
        else:
            logger.error("Failed to %s after %d retries.", desc, retry_count)
            raise Exception("Failed to %s after %d retries.", desc, retry_count)
