'''
Input: raw timelines (where a single user's info can be in multiple timelines due to rate limits) in JSON format
output: 1. Slimdown: slimmed down version of raw timeline in JSON format and compressed 
            It filters out redundant user info (in user as well as original poster of a retweet), add crawl time, and add user's "snowball_wave".
            This output will be permanently stored in the Cornell's server cluster.
        2. Essential: info in timeline essential for network analysis in JSON format. Will be stored on S3
        3. Aggregate: aggregated tweet information from the essential. Store on S3.
'''
import time
import boto
import json
import gzip
import bz2
from datetime import datetime
import signal, time
import conf
from country import crawler_country_classifier
from random import randint
#from pymongo import MongoClient
if conf.DATABASE == 'HDX':
    import hyperdex.client
import logging
import gc

logger = logging.getLogger(__name__)
class Slimdown():

    def __init__(self):
	self.client = hyperdex.client.Client(conf.HYPERDEX_IP, conf.HYPERDEX_PORT) #HyperDex
        self.sdbconn = None
        self.domain = None

    def get_crawl_time_and_snowball_wave(self, user_id, screen_name):
        """
            Get the crawl_time and snowball_wave for user_id or screen_name.
            Because Amazon SQS automatically deletes messages older than 4 days,
            the crawl process isn't always breadth-first.

            Therefore, snowball_wave shouldn't be used to determine the edge of
            the network.
        """
	if conf.DATABASE == 'MDB':
	    item = list(self.domain.find({'_id':user_id})) or list(self.domain.find({'_id':screen_name}))
            if len(item)!=0:
                return item[0].get('crawled_utc', str(datetime.utcnow())), item[0].get('depth', 0)
            else:
                return str(datetime.utcnow()), 0
        if conf.DATABASE == 'SDB':
            item = self.domain.get_item(user_id) or self.domain.get_item(screen_name)
            if item:
                return item.get('crawled_utc', str(datetime.utcnow())), item.get('depth', 0)
            else:
                return str(datetime.utcnow()), 0
        if conf.DATABASE == 'HDX':
	    item = None
	    count = 0
	    while True:
                try:
		    item = helper(self.client,self.domain, str(user_id))
                except Exception as e:
		    count = count + 1
		    if count == 10:
			logger.error('failed at get_crawl_time_and_snowball_wave')
			break
                    logger.error('Error happens at get_crawl_time_and_snowball_wave, detail: %s',str(e))
		    self.client = None
                    time.sleep(randint(100,250))
		    self.client = hyperdex.client.Client(conf.HYPERDEX_IP, conf.HYPERDEX_PORT)
                    continue
                break
	    gc.collect()
            if item!=None:
                return item.get('crawled_utc', str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f"))), item.get('depth', 0)
            else:
                return str(datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S.%f")), 0

    def remove_unnecessary_in_user(self, user_info): #tt['user'] or tt['retweeted_status']['user']
        del user_info['profile_background_color']
        del user_info['profile_background_image_url']
        del user_info['profile_background_image_url_https']
        del user_info['profile_background_tile']
        del user_info['profile_link_color']
        del user_info['profile_sidebar_border_color']
        del user_info['profile_sidebar_fill_color']
        del user_info['profile_text_color']
        del user_info['profile_use_background_image']
        return user_info
    
    def slimdown_timeline(self, tline): #timeline_string = [ {tweet1}, {tweet2}... ]. Assumes ALL tweet chuncks are in this string.
        slim = {'tweets':[], 'user': None, 'crawl_time':None, 'snowball_wave': None }
        for tt in tline:   
            if slim['user'] is None:
                slim['user'] = self.remove_unnecessary_in_user( tt['user'] )
            del tt['user']
            

            if 'retweeted_status' in tt:
                tt['retweeted_status']['user'] = self.remove_unnecessary_in_user( tt['retweeted_status']['user'] )   
            slim['tweets'].append( tt )
        return slim
    
    
class Essential():
    def user_parse(self, tt):
        user = {'created_at': tt['user']['created_at'], \
                'profile_image_url': tt['user']['profile_image_url'], \
                'description': tt['user']['description'], \
                'followers_count': tt['user']['followers_count'], \
                'favourites_count': tt['user']['favourites_count'], \
                'friends_count': tt['user']['friends_count'], \
                'geo_enabled': tt['user']['geo_enabled'], \
                'lang': tt['user']['lang'], \
                'location': tt['user']['location'], \
                'time_zone': tt['user']['time_zone'], \
                'statuses_count' : tt['user']['statuses_count'], \
                'id_str': tt['user']['id_str'], \
                'utc_offset': tt['user']['utc_offset'],\
                'verified': tt['user']['verified'], \
                'name': tt['user']['name'] }  
        return user
       
    def tweet_parse(self, tt):
        par = {} #parsed tweet container
        par['created_at'] = tt['created_at']      # account creating date and time
        par['contributors'] = tt['contributors'] 
        par['entities'] = tt['entities']          # hashtags, symbols, urls, user mentions
        par['coordinates'] = tt['coordinates']    # GPS location of tweet. Check whether null value is "None" or "null".
        par['place'] = tt['place']                # tweet location info provided by user from a dropdown menu
        par['id_str'] = tt['id_str']              # tweet ID
        par['in_reply_to_status_id_str'] = tt['in_reply_to_status_id_str'] #If the tweet is a reply to an original tweet, record the original tweet's ID
        par['in_reply_to_user_id_str'] = tt['in_reply_to_user_id_str']     # userID X to whom focal tweet replies 
        par['lang']= tt['lang']       # language of tweet identified by Twitter
        par['text'] = tt['text']      #tweet text
        return par
    
    def retweeted_status_parse(self, tt):
        #Parses tweet portion and user portion
        par = self.tweet_parse(tt['retweeted_status'])
        par['user'] = self.user_parse( tt['retweeted_status'] )
        return par
     
    def parse_essential(self, slim):
        d = {'tweets':[], 'user': self.user_parse(slim), 'crawl_time': slim['crawl_time'], 'snowball_wave': slim['snowball_wave'] }
        
        for tt in slim['tweets']:
            par = self.tweet_parse(tt)
            if 'retweeted_status' in tt:  
                par['retweeted_status'] = self.retweeted_status_parse(tt)
            else:
                par['retweeted_status'] = None
                
            d['tweets'].append( par )
        return d


class Aggregate():
    def agg_container(self, tline):  
        combined = {}
        combined['crawl_time'] = tline['crawl_time']
        combined['snowball_wave'] = tline['snowball_wave']
        
        user_info = tline['user']
        
        #user info
        combined['user_id'] = user_info['id_str']
        combined['user_location'] = {user_info['location']: 1}
        combined['user_time_zone'] = {user_info['time_zone']: 1}
        combined['utc_offset'] = {user_info['utc_offset']: 1}
        combined['statuses_count'] = {user_info['statuses_count']: 1}
        combined['followers_count'] = {user_info['followers_count']: 1}
        combined['friends_count'] = {user_info['friends_count']: 1}
        combined['user_lang'] = {user_info['lang']: 1}
        combined['user_created_at'] = {user_info['created_at']: 1}
        combined['name'] = {user_info['name']: 1}
        combined['description'] = {user_info['description']: 1}
        combined['profile_image_url'] = {user_info['profile_image_url']: 1}
        
        #tweet info
        combined['tweet_lang'] = {}
        combined['tweet_location'] = {}     #location from dropdown menu
        combined['tweet_coordinates'] = []  #location from GPS info
        combined['mentioned'] = {}
        #combined['tweet_id'] = {}
        combined['num_reply_tweets'] = 0    #Count number of focal user's tweets that are replies
        combined['num_tweets_with_mention'] = 0
        combined['num_tweets_in_data'] = 0
        combined['in_reply_to_user_id_str'] = {}
        combined['retweets'] = { 'original_tweet_user_id':{}, 'all_mentioned_in_retweets':{}, \
                                'additionally_mentioned_in_retweets':{}, 'num_retweets_in_data': 0, \
                                'in_reply_to_user_id_str_in_original_tweets': {}, \
                                'num_retweets_with_additionally_mentioned': 0} #retweet_ids={ retweet_id_string_in_original_user's: retweet_id_string_in_current_user's}
        return combined

    
    def agg_tweet(self, tt, combined): #tt = one raw tweet , combined= all tweets from single user combined (dictionary)
        if not tt['retweeted_status']: #if not a retweet     
            try:
                if tt['lang'] not in combined['tweet_lang']:
                    combined['tweet_lang'][tt['lang']] = 0
                combined['tweet_lang'][tt['lang']] += 1
            except:
                raise
            
            try:
                if tt['place']:
                    if tt['place']['country_code'] not in combined['tweet_location']:
                        combined['tweet_location'][ tt['place']['country_code'] ] = 0
                    combined['tweet_location'][ tt['place']['country_code'] ] += 1
            except:
                raise
            
            try:
                if tt['coordinates']:
                    combined['tweet_coordinates'].append( tt['coordinates']['coordinates'] )
            except:
                raise
            
            try:
                unique_mentioned = [] #Only count unique IDs once. Some tweets contain the same ID multiple times.
                for mentioned in tt['entities']['user_mentions']:
                    if mentioned['id_str'] not in unique_mentioned:
                        unique_mentioned.append(mentioned['id_str'])
                        if mentioned['id_str'] not in combined['mentioned']:
                            combined['mentioned'][ mentioned['id_str'] ] = 0
                        combined['mentioned'][ mentioned['id_str'] ] += 1   
                if len(tt['entities']['user_mentions']) > 0: 
                    combined['num_tweets_with_mention'] += 1
            except:
                raise
            
            #in reply to ID. There always seems to be only one user ID for this field.
            #e.g.   'in_reply_to_user_id_str': '81401640'
            try:
                if tt['in_reply_to_user_id_str']:
                    combined['num_reply_tweets'] += 1 
                    if tt['in_reply_to_user_id_str'] not in combined['in_reply_to_user_id_str']:
                        combined['in_reply_to_user_id_str'][ tt['in_reply_to_user_id_str'] ] = 0
                    combined['in_reply_to_user_id_str'][ tt['in_reply_to_user_id_str'] ] += 1
            except:
                raise
            
            #combined['tweet_id'][tt['id_str']] = 1 #Store the tweet ID.  
            combined['num_tweets_in_data'] += 1
            
        
        else: #if it is a retweet
            #key= Originator's user ID of retweet, value= frequency of retweeting from user ID
            try:
                if tt['retweeted_status']['user']['id_str'] not in combined['retweets']['original_tweet_user_id']:
                    combined['retweets']['original_tweet_user_id'][ tt['retweeted_status']['user']['id_str'] ] = 0
                combined['retweets']['original_tweet_user_id'][ tt['retweeted_status']['user']['id_str'] ] += 1
            except:
                raise
            
            #dict of all mentions in original tweet (= original poster + mentions made by the original poster + mentions made by focal user)
            try:
                unique_mentioned_rt = []
                for mentioned in tt['entities']['user_mentions']:
                    if mentioned['id_str'] not in unique_mentioned_rt:
                        unique_mentioned_rt.append(mentioned['id_str'])
                        if mentioned['id_str'] not in combined['retweets']['all_mentioned_in_retweets']:
                            combined['retweets']['all_mentioned_in_retweets'][ mentioned['id_str'] ] = 0
                        combined['retweets']['all_mentioned_in_retweets'][ mentioned['id_str'] ] += 1
            except:
                raise   
                
            #dict of mentioned user IDs that were added by focal user. This dictionary excludes the user who originally created the tweet and those who were mentioned by original tweeter.
            try:
                if len( tt['entities']['user_mentions'] ) > 0:
                    retweet_mentioned = set( [ i['id_str'] for i in tt['entities']['user_mentions'] ] ) #a set of mentioned ids in the ego's retweet info.
                    
                    #Create a set with user ids which include original mentioned ids and original tweet's user id 
                    if len( tt['retweeted_status']['entities']['user_mentions'] ) > 0:
                        original_user_and_mentioned_id = [ i['id_str'] for i in tt['retweeted_status']['entities']['user_mentions'] ]
                        original_user_and_mentioned_id.append( tt['retweeted_status']['user']['id_str'] )
                        original_user_and_mentioned_id = set( original_user_and_mentioned_id )
                    else:
                        original_user_and_mentioned_id = set([ tt['retweeted_status']['user']['id_str'] ])   
                    added_mention_ids = retweet_mentioned - original_user_and_mentioned_id
                    for each in added_mention_ids:
                        if each not in combined['retweets']['additionally_mentioned_in_retweets']:
                            combined['retweets']['additionally_mentioned_in_retweets'][ each ] = 0
                        combined['retweets']['additionally_mentioned_in_retweets'][ each ] += 1 
                    if len(added_mention_ids) > 0:
                        combined['retweets']['num_retweets_with_additionally_mentioned'] += 1
            except:
                raise     
            
            #dict of user IDs that original poster replied to. (ex. A tweets. B replies to A. C retweets B's reply. then A will be in 'in_reply_to_user_id_str_in_original_tweet'.)
            try:
                if len(tt['retweeted_status']['in_reply_to_user_id_str']) > 0:    
                    if tt['retweeted_status']['in_reply_to_user_id_str'] not in combined['retweets']['in_reply_to_user_id_str_in_original_tweets']:
                        combined['retweets']['in_reply_to_user_id_str_in_original_tweets'][ tt['retweeted_status']['in_reply_to_user_id_str'] ] = 0
                    combined['retweets']['in_reply_to_user_id_str_in_original_tweets'][ tt['retweeted_status']['in_reply_to_user_id_str'] ] += 1 
            except:
                raise
            combined['retweets']['num_retweets_in_data'] += 1
            #dict of languages of retweets. tt['retweeted_status'][retweet 'lang']
            #dict of countries from which tweets were geotagged. tt['retweeted_status']['place']['country_code']
        return combined    

    def agg_tweets( self, essential_info ): #input is timeline information. 
        check1 = {} #check for duplicate tweets
        agg = self.agg_container( essential_info )
        for tt in essential_info['tweets']:
            if tt[ 'id_str' ] not in check1:
                check1[ tt[ 'id_str' ] ] = 1
                try: 
                    agg = self.agg_tweet(tt, agg)
                except: #Ignore tweets that are ill-formatted.
                    pass
        return agg


class WriteToFile():
    
    def __init__(self, rawfile, slimfile, essentialfile, aggfile):
        self.rawfile = rawfile
	self.slimfile = slimfile
        self.slimfile_upload = slimfile+'_upload'
        self.essentialfile = essentialfile
        self.essentialfile_upload = essentialfile+'_upload'
        self.aggfile = aggfile
        self.aggfile_upload = aggfile+'_upload'
        
    def run(self): #Writes slim, essential, and aggregate to files.
        sd = Slimdown()
        es = Essential()
        ag = Aggregate()

        ids = {}
        is_targeted = {}
	
	with gzip.open(self.rawfile, 'r') as rawdata, \
                bz2.BZ2File(self.slimfile_upload, 'w') as s_out, \
                bz2.BZ2File(self.essentialfile_upload, 'w') as e_out, \
                bz2.BZ2File(self.aggfile_upload, 'w') as a_out:

            for line in rawdata:
		print type(line)
		print len(line)
                line = line.strip()
                try:
                    tline = json.loads(line)
                except:
                    try:
                        tline = eval(line)
                    except:
                        tline = None

                if tline:
		    print type(tline)
		    #print tline
		    print len(tline)
		    time.sleep(3000)
                    slim = sd.slimdown_timeline(tline)
                    essential = es.parse_essential(slim)
                    aggregate = ag.agg_tweets(essential) #Aggregation of essential at the user level

                    s_out.write(json.dumps(slim) + '\n')
                    e_out.write(json.dumps(essential)+'\n')
                    a_out.write(json.dumps(aggregate)+'\n') 
            return ids, is_targeted

def helper(client,domain, str_user_id):
    item = client.get(domain, str_user_id)
    return item

if __name__ == "__main__":
    import time
    raw_file = "/Users/shishong/Documents/testFiles/TwitterRawTimeline/egypt/ip-10-62-94-94.ec2.internal20130916224128573105" #Raw timeline file
    slim_file = "/Users/shishong/Documents/twitter/data/egypt_tweets_20130917/parsed/slimmed/egypt_slim.txt"
    essential_file = "/Users/shishong/Documents/twitter/data/egypt_tweets_20130917/parsed/essential/egypt_essential.txt"
    agg_file = "/Users/shishong/Documents/twitter/data/egypt_tweets_20130917/parsed/aggregated/egypt_aggregated.txt"
    
    w = WriteToFile( raw_file, slim_file, essential_file, agg_file )
    time0 = time.time()
    w.run()
    time1 = time.time()

    print "time to completion:", (time1-time0)/60., "minutes"
