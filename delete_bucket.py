import boto
import conf
import sys

no_confirm = '--no-confirm' in sys.argv
if no_confirm:
    sys.argv.remove('--no-confirm')

conn = boto.connect_s3(conf.AWS_KEY, conf.AWS_SECRET)

def remove(name):
    bucket = conn.get_bucket(name)
    print 'deleting bucket %s...\r' % bucket.name,
    sys.stdout.flush()
    keys = []
    for index, key in enumerate(bucket.list()):
        keys.append(key.name)
        if len(keys) == 1000:
            print 'deleting bucket %s... %d\r' % (bucket.name, index),
            sys.stdout.flush()
            bucket.delete_keys(keys)
            keys = []

    bucket.delete_keys(keys)
    conn.delete_bucket(name)
    print 'bucket %s is deleted successfully.             ' % bucket.name

matched_buckets = []
for i in range(1, len(sys.argv[1])):
    try:
        bucket_name = sys.argv[i]
        if '*' not in bucket_name:
            remove(bucket_name)
        else:
            if bucket_name.find('*') != len(bucket_name)-1:
                print('Pattern only support trailing *, such as "egypt*", "prod*".')
                exit(-1)
            bucket_name = bucket_name[0:len(bucket_name)-1]
            buckets = conn.get_all_buckets()
            for b in buckets:
                if b.name.startswith(bucket_name):
                    matched_buckets.append(b)
    except Exception, e:
        print(str(e))

if not no_confirm:
    print 'Buckets matched with pattern(s):'
    for i in range(0, len(matched_buckets)-1, 2):
        print '\t%s\t%s%s' % (matched_buckets[i].name,
                              '\t'*(3-len(matched_buckets[i].name)/8),
                              matched_buckets[i+1].name)
    if len(matched_buckets) % 2 == 1:
        print '\t%s' % matched_buckets[-1].name
    print ''
    s = raw_input("Continue deleting buckets? (y/n)")
    if s not in ['Y', 'y']:
        exit(0)

for b in matched_buckets:
    remove(b)