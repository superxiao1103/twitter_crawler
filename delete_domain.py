import boto
import conf
import sys

no_confirm = '--no-confirm' in sys.argv
if no_confirm:
    sys.argv.remove('--no-confirm')

conn = boto.connect_sdb(conf.AWS_KEY, conf.AWS_SECRET)

def remove(name):
    conn.delete_domain(name)
    print '%s is deleted successfully.' % name

matched_domains = []
for i in range(1, len(sys.argv[1])):
    try:
        domain_name = sys.argv[i]
        if '*' not in domain_name:
            remove(domain_name)
        else:
            if domain_name.find('*') != len(domain_name)-1:
                print('Pattern only support trailing *, such as "egypt*", "prod*".')
                exit(-1)
            domain_name = domain_name[0:len(domain_name)-1]
            domains = conn.get_all_domains()
            for d in domains:
                if d.name.startswith(domain_name):
                    matched_domains.append(d)
    except Exception, e:
        print(str(e))

if not no_confirm:
    print 'Domains matched with pattern(s):'
    for i in range(0, len(matched_domains)-1, 2):
        print '\t%s\t%s%s' % (matched_domains[i].name,
                              '\t'*(3-len(matched_domains[i].name)/8),
                              matched_domains[i+1].name)
    if len(matched_domains) % 2 == 1:
        print '\t%s' % matched_domains[-1].name
    print ''
    s = raw_input("Continue deleting domains? (y/n)")
    if s not in ['Y', 'y']:
        exit(0)

for b in matched_domains:
    remove(b)