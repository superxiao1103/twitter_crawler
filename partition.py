"""
Represents a PartitionManager that manages a number of SDB Domains partitioned by item name.
"""
import os
import json
import logging
import logging.config
import sys
import time
import conf
from random import randint
from utils import setup_logging_config, retry
if conf.DATABASE == 'HDX':
    import hyperdex.client
import signal, time
from pymongo import MongoClient

if __name__ == '__main__':
    if not os.path.exists('log'):
        os.makedirs('log')
    with open('logging_config.json', 'rt') as logging_config:
        config = setup_logging_config(json.load(logging_config))
    logging.config.dictConfig(config)

logger = logging.getLogger(__name__)

class PartitionManager:
    def __init__(self, sdb_conn, base_name, count_partitions):
        self.sdb_conn = sdb_conn
        self.base_name = base_name
        self.count_partitions = count_partitions
        self.dom = {}
	self.client = None
	self.clients = list()
	if conf.DATABASE == 'HDX':
	    for ip in conf.HYPERDEX_IP_LIST:
	        self.clients.append(hyperdex.client.Client(ip, conf.HYPERDEX_PORT))
	if conf.DATABASE == 'MDB':
	    for ip in conf.MONGODB_IP_LIST:
		self.clients.append(MongoClient(ip, 2000)['testDB']['prod1311_timeline'])

    def _get_domain_id(self, item_name, n_partitions=None):
        """
        Get domain id (the partition key) for item_name.
        """
        if not item_name:
            raise ValueError('item_name should not be None.')
        elif type(item_name) is not str:
            item_name = str(item_name)

        n_partitions = n_partitions if n_partitions else self.count_partitions
        return hash(item_name) % n_partitions

    def _get_domain_by_item(self, item_name, n_partitions=None):
        """
        Get the SDB Domain instance for item_name.
        """
        id = self._get_domain_id(item_name, n_partitions)
        return self._get_domain(id)

    def _get_domain(self, domain_id):
        name = None
        if conf.DATABASE == 'SDB':
            if domain_id not in self.dom:
                name = "%s_%d" % (self.base_name, domain_id) if self.count_partitions > 1 else self.base_name
                self.dom[domain_id] = self.sdb_conn.lookup(name)
        if conf.DATABASE == 'HDX' or 'MDB':
            name = self.base_name
	    self.dom[domain_id] = name #for Hyperdex
        return self.dom[domain_id]

    def put_attributes(self, item_name, attributes,
                       replace=True, expected_value=None):
        """
        Store attributes for a given item to its partition.
        """
        d = self._get_domain_by_item(item_name)
        if conf.DATABASE == 'SDB':
            return retry(lambda: d.put_attributes(item_name, attributes, replace, expected_value),
                         "put attributes to %s" % d.name)
	if conf.DATABASE == 'MDB':
	    #self.clients[ip_index]
	    #d.update({'_id':str(item_name)}, {"$set":dict(attributes)}, upsert=True)
	    ip_index = self._get_domain_id(item_name)
            ip = conf.MONGODB_IP_LIST[ip_index]
            count = 0
            while True:
                try:
                    self.client = self.clients[ip_index]
		    self.client.update({'_id':str(item_name)}, {"$set":dict(attributes)}, upsert=True)
                    self.client = None
                    return
                except Exception as e:
                    count = count + 1
                    if count>=10:
                        logger.error('failed to put_attributes')
                        return
                    logger.error('Error happens at put_attributes on %s, detail: %s',ip,str(e))
                    time.sleep(randint(100,250))
        if conf.DATABASE == 'HDX':
	    ip_index = self._get_domain_id(item_name)
	    ip = conf.HYPERDEX_IP_LIST[ip_index]
	    count = 0
	    while True:
                try:
		    self.client = self.clients[ip_index]
                    helper1(self.client,d,str(item_name),dict(attributes))
		    self.client = None
		    return
                except Exception as e:
		    count = count + 1
		    if count>=10:
			logger.error('failed to put_attributes')
			return
                    logger.error('Error happens at put_attributes on %s, detail: %s',ip,str(e))
		    time.sleep(randint(100,250))

    def batch_put_attributes_all(self, items, replace=True):
        """
        Store attributes for multiple items.
        The items are split to different partitions and multiple calls,
          each call consists of at most 25 items, per AWS limitation.

        return: yields result of each call.
        """
        bucket = {}
        for k in items.keys():
            id = self._get_domain_id(k)
            if id not in bucket:
                bucket[id] = {}
            bucket[id][k] = items[k]

        results = []
        for id in bucket.keys():
            d = self._get_domain(id)
	    if conf.DATABASE == 'MDB':
                ip = conf.MONGODB_IP_LIST[id]
                self.client = self.clients[id]
                for i in bucket[id].keys():
                    count = 0
                    while True:
                        try:
			    self.client.update({'_id':str(i)},{"$set":dict(bucket[id][i])},upsert=True)
                        except Exception as e:
                            count = count + 1
                            if count >= 10:
                                logger.error('failed to batch_put_attributes_all')
                                break
                            logger.error('Error happens at batch_put_attributes_all on %s, detail: %s',ip,str(e))
                            time.sleep(randint(100,250))
                            continue
                        break
            if conf.DATABASE == 'HDX':
		ip = conf.HYPERDEX_IP_LIST[id]
		self.client = self.clients[id]
		for i in bucket[id].keys():
		    count = 0
		    while True:
                	try:
			    helper2(self.client,d,str(i),dict(bucket[id][i]))
                	except Exception as e:
			    count = count + 1
			    if count >= 10:
				logger.error('failed to batch_put_attributes_all')
				break
                            logger.error('Error happens at batch_put_attributes_all on %s, detail: %s',ip,str(e))
			    time.sleep(randint(100,250))
                            continue
                        break

		self.client = None
            if conf.DATABASE == 'SDB':
                for i in range(0, len(bucket[id]), 25):
                    r = retry(lambda: d.batch_put_attributes(dict(bucket[id].items()[i:i+25]), replace), 
                              "batch put attributes to domain %s" % d.name)
                    results.append(r)
	results = None
	bucket = None

    def get_item(self, item_name, consistent_read=False):
        """
        Retrieves an item from its domain, along with all of its attributes.
        """
        d = self._get_domain_by_item(item_name)
        try:
            if conf.DATABASE == 'HDX':
		ip_index = self._get_domain_id(item_name)
                ip = conf.HYPERDEX_IP_LIST[ip_index]
		temp = {}
		count = 0
		while True:
                    try:
			self.client = self.clients[ip_index]
			temp = self.client.get(d, str(item_name))
			self.client = None
			break
                    except Exception as e:
			count = count + 1
			if count >= 10:
			    logger.error('failed to get_item')
			    break
                        logger.error('Error happens at get_item on %s, detail: %s',ip,str(e))
			time.sleep(randint(100,250))
		return temp
	    if conf.DATABASE == 'MDB':
                ip_index = self._get_domain_id(item_name)
                ip = conf.MONGODB_IP_LIST[ip_index]
                temp = {}
                count = 0
                while True:
                    try:
                        self.client = self.clients[ip_index]
			if len(list(self.client.find({'_id':str(item_name)})))!=0:
			    temp = list(self.client.find({'_id':str(item_name)}))[0]
                            self.client = None
                        break
                    except Exception as e:
                        count = count + 1
                        if count >= 10:
                            logger.error('failed to get_item')
                            break
                        logger.error('Error happens at get_item on %s, detail: %s',ip,str(e))
                        time.sleep(randint(100,250))
                return temp
            if conf.DATABASE == 'SDB':
                return d.get_item(item_name, consistent_read)
	except Exception as e:
	    return {}

def helper1(client, d, str_item_name, dict_attr):
    client.put(d,str_item_name,dict_attr)

def helper2(client,d,str_i,dict_bucket_id_i):
    client.put(d,str_i,dict_bucket_id_i) #for HyperDex

def helper3(client,d, str_item_name):
    temp = client.get(d, str_item_name)
    return temp

if __name__ == '__main__':
    import boto.sdb
    import conf

    if sys.argv[1] == 'migrate':
        migrate_main()
