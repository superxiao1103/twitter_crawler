"""
This is the configuration module for the crawler.

Shared configurations are defined in __init__.py.
Environment specific configurations are defined in conf.dev.py and conf.prod.py.

Set environment variable 'DEPLOYMENT_NAME' to 'PROD' to enable settings for production.
"""

import os

deployment = os.environ.get('DEPLOYMENT_NAME')
if deployment == 'PROD':
    from prod_settings import *
elif deployment == 'EGYPT':
    from egypt_settings import *
else:
    from dev_settings import *

# SDL LAB Key
AWS_KEY = 'AKIAJE5BIEFPSIOGPNYQ'
AWS_SECRET = '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'

# EGYPTIAN CRAWL KEY
#AWS_KEY = 'AKIAJHIQYPL7N3OTXNHQ'
#AWS_SECRET = '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'

MAX_FAILURES = 5

CRAWL_TYPE_FRIENDS = 'friends'
CRAWL_TYPE_FOLLOWERS = 'followers'
CRAWL_TYPE_TIMELINE = 'tweets'

TEMP_FOLDER = 'temp'
TEMP_FILE_FRIENDS = 'friend_file'
TEMP_FILE_FOLLOWERS = 'follower_file'
TEMP_FILE_TIMELINE = 'tweet_file'
TEMP_FILE_PARSED_TIMELINE = 'parsed_tweets'

SQS_RETENTION_DAYS = 14

