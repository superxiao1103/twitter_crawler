"""
Configuration for EGYPT PROD environment.
"""

DEPLOYMENT_NAME = 'EGYPT'
DEBUG = False

NOTIFICATION_FROM = 'twitter_crawler@sdlcnp.soc.cornell.edu'
NOTIFICATION_TO = 'positivelyskewed.sf@gmail.com, pp286@cornell.edu, yz672@cornell.edu'

SDB_REGION = 'us-east-1'

SDB_DOMAIN_FRIENDS = 'egypt1311_friends'
SDB_DOMAIN_FOLLOWERS = 'egypt1311_followers'
SDB_DOMAIN_TIMELINE = 'egypt1311_timeline'
SDB_DOMAIN_META = 'egypt1311_meta'
SDB_DOMAIN_ACCOUNTS = 'accounts'  # Reuse existing accounts in PROD

S3_BUCKET_FRIENDS = 'egypt1311-friends'
S3_BUCKET_FOLLOWERS = 'egypt1311-followers'
S3_BUCKET_TIMELINE = 'egypt1311-timeline'
S3_BUCKET_PARSED_TIMELINE = 'egypt1311-timeline-parsed'

QUEUE_FRIENDS = 'egypt1311_friends'
QUEUE_FOLLOWERS = 'egypt1311_followers'
QUEUE_TIMELINE = 'egypt1311_timeline'
QUEUE_ACCOUNTS = 'accounts'
S3_BUCKET_SLIM = 'egypt1311-timeline-slim'
S3_BUCKET_ESSENTIAL = 'egypt1311-timeline-essential'
S3_BUCKET_AGGREGATE = 'egypt1311-timeline-aggregate'

DOMAIN_PARTITION_COUNT = 1

COUNTRY_CLASSIFIER_ON = False

MAX_QUEUE_SIZE = 2000000

BATCH_SIZE = 500000
BATCH_TIMEOUT = 3600

MAX_WORKER_THREADS = 3

MAX_DEPTH = 1
