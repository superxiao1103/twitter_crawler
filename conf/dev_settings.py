"""
Configuration for DEV environment.
"""

DEPLOYMENT_NAME = 'DEV'
DEBUG = False

NOTIFICATION_FROM = 'twitter_crawler@sdlcnp.soc.cornell.edu'
NOTIFICATION_TO = ''

SDB_REGION = 'us-east-1'

SDB_DOMAIN_FRIENDS = 'dev_friends'
SDB_DOMAIN_FOLLOWERS = 'dev_followers'
SDB_DOMAIN_TIMELINE = 'dev_timeline'
SDB_DOMAIN_META = 'dev_meta'
SDB_DOMAIN_ACCOUNTS = 'accounts'

S3_BUCKET_FRIENDS = 'dev-friends'
S3_BUCKET_FOLLOWERS = 'dev-followers'
S3_BUCKET_TIMELINE = 'dev-timeline'
S3_BUCKET_SLIM = 'dev-timeline-slim'
S3_BUCKET_ESSENTIAL = 'dev-timeline-essential'
S3_BUCKET_AGGREGATE = 'dev-timeline-aggregate'

DOMAIN_PARTITION_COUNT = 2

QUEUE_FRIENDS = 'dev_friends'
QUEUE_FOLLOWERS = 'dev_followers'
QUEUE_TIMELINE = 'dev_timeline'
QUEUE_ACCOUNTS = 'accounts'

COUNTRY_CLASSIFIER_ON = True

MAX_QUEUE_SIZE = 1000

BATCH_SIZE = 500
BATCH_TIMEOUT = 120

MAX_WORKER_THREADS = 3

MAX_DEPTH = 5