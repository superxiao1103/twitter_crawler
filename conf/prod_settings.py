"""
Configuration for PROD environment.
"""
DATABASE = 'SDB'
#DATABASE = 'MDB'
#DATABASE = 'HDX'

DEPLOYMENT_NAME = 'PROD'
DEBUG = False

NOTIFICATION_FROM = 'twitter_crawler@sdlcnp.soc.cornell.edu'
NOTIFICATION_TO = ['positivelyskewed.sf@gmail.com', 'pp286@cornell.edu', 'yz672@cornell.edu']

MONGODB_IP = '54.203.153.28'
HYPERDEX_IP = '10.231.169.204'
#HYPERDEX_IP_LIST = ['10.231.169.204','10.36.76.26']
HYPERDEX_IP_LIST = ['10.17.16.125']
MONGODB_IP_LIST = ['10.233.151.97']
HYPERDEX_PORT = 2000

SDB_REGION = 'us-east-1'

SDB_DOMAIN_FRIENDS = 'prod1311_friends'
SDB_DOMAIN_FOLLOWERS = 'prod1311_followers'
SDB_DOMAIN_TIMELINE = 'prod1311_timeline'
SDB_DOMAIN_META = 'prod1311_meta'
SDB_DOMAIN_ACCOUNTS = 'accounts'

S3_BUCKET_FRIENDS = 'prod1311-friends'
S3_BUCKET_FOLLOWERS = 'prod1311-followers'
S3_BUCKET_TIMELINE = 'prod1311-timeline'
''''
S3_BUCKET_SLIM = 'prod1311-timeline-slim'
S3_BUCKET_ESSENTIAL = 'prod1311-timeline-essential'
S3_BUCKET_AGGREGATE = 'prod1311-timeline-aggregate'
'''
S3_BUCKET_SLIM      = 'test-xiao-slim'
S3_BUCKET_ESSENTIAL = 'test-xiao-essential'
S3_BUCKET_AGGREGATE = 'test-xiao-aggregate'

SQS_REGION = "us-east-1"

QUEUE_FRIENDS = 'prod1311_friends'
QUEUE_FOLLOWERS = 'prod1311_followers'
QUEUE_TIMELINE = 'prod1311_timeline'
#QUEUE_TIMELINE = 'testing'
QUEUE_ACCOUNTS = 'accounts'

DOMAIN_PARTITION_COUNT = 220
DOMAIN_PARTITION_COUNT_HDX = 2

COUNTRY_CLASSIFIER_ON = True

MAX_QUEUE_SIZE = 2000000

BATCH_SIZE = 500000
#BATCH_SIZE = 5000
BATCH_TIMEOUT = 3600    # Force to store local results every 1 hours
#BATCH_TIMEOUT = 60

MAX_WORKER_THREADS = 1
ADD_WORKER_THREADS = 0

MAX_DEPTH = 1000000000
NUM_THREADS = 1
ADD_THREADS = 0
