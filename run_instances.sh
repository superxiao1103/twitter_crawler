#!/bin/bash

# Execute getopt
ARGS=$(getopt -o a:s:e:b:i:r: -l "ami:,start:,end:,bid:,id:,region:" -n "getopt.sh" -- "$@");

#Bad arguments
if [ $? -ne 0 ];
then
    exit 1
fi

REGION=us-east-1
AMI=ami-512a0938

BID=0.03
FIRST=1
LAST=1

eval set -- "$ARGS";

while true; do
    case "$1" in
        -r|--region)
            shift;
            if [ -n "$1" ]; then
                echo "-r used: $1";
                REGION=$1
                shift;
            fi
         ;;
        -a|--ami)
            shift;
            if [ -n "$1" ]; then
                echo "-a used: $1";
                AMI=$1
                shift;
            fi
         ;;
        -s|--start)
            shift;
            if [ -n "$1" ]; then
                echo "-s used: $1";
                FIRST=$1
                shift;
            fi
        ;;
        -e|--end)
            shift;
            if [ -n "$1" ]; then
                echo "-e used: $1";
                LAST=$1
                shift;
            fi
        ;;
        -i|--id)
            shift;
            if [ -n "$1" ]; then
                echo "-i used: $1";
                FIRST=$1
                LAST=$1
                shift;
            fi
        ;;
        -b|--bid)
            shift;
            if [ -n "$1" ]; then
                echo "-b used: $1";
                BID=$1
                shift;
            fi
        ;;
        --)
            shift;
            break;
        ;;
    esac
done

for ((ID=$FIRST; ID<=$LAST; ID++))
do
    # us-east-1
    ec2-request-spot-instances -n 1 -r persistent -p $BID -t t1.micro --key soctweets --group default --region $REGION -d $ID $AMI
    sleep 2s

    # us-west-1
    #ec2-request-spot-instances -n 1 -r persistent -p $BID -t t1.micro --key soctweets-us-west-1 --region $REGION -d $ID $AMI
    #
done
